package app

import (
	"fmt"
	"bitbucket.org/ivan-iver/revel"
)

func init() {
	revel.OnAppStart(func() {
		fmt.Println("Go to /@tests to run the tests.")
	})
}
