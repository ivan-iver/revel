package controllers

import "bitbucket.org/ivan-iver/revel"

type Application struct {
	*revel.Controller
}

func (c Application) Index() revel.Result {
	return c.Render()
}
