package controllers

import "bitbucket.org/ivan-iver/revel"

type App struct {
	*revel.Controller
}

func (c App) Index() revel.Result {
	return c.Render()
}
